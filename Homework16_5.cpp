#include <iostream>

int main()
{
    const int currentDay = 4;
    const int N = 20;
    int array[N][N];

    // Init array
    for (int i = 0; i < N; ++i)
        for (int j = 0; j < N; j++)
            array[i][j] = i + j;

    // Print array with sum
    for (int i = 0, sum = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            const int val = array[i][j];
            if (val < 10)
                std::cout << ' ';
            std::cout << val << ' ';

            sum += val;
        }

        if (i == currentDay % N)
        {
            std::cout << "sum: " << sum;
            sum = 0;
        }
        std::cout << '\n';
    }
}
